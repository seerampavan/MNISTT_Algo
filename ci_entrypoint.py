#!/usr/bin/python3

import os, json
from algorithmia_ci import build_wait, get_build, test_algo, publish_algo

if __name__ == "__main__":
    api_key = os.getenv("API_KEY")
    api_address = os.getenv("API_ADDRESS")
    commit_hash = os.getenv("CI_COMMIT_SHA")
    publish_schema = "minor"

    if not api_key:
        raise Exception("field 'api_key' not defined in workflow")
    if not api_address:
        raise Exception("field 'api_address' not defined in workflow")
    if not publish_schema:
        raise Exception("field 'version_schema' not defined in workflow")

    with open("algorithmia.conf") as f:
        config_data = json.load(f)
    with open("TEST_CASES.json") as f:
        case_data = json.load(f)
    algo_name = "{}/{}".format(config_data['username'], config_data['algoname'])
    build_wait(api_key, api_address, algo_name, commit_hash)
    algo_hash = get_build(api_key, api_address, algo_name)
    test_algo(api_key, api_address, case_data, algo_name, algo_hash)
    publish_algo(api_key, api_address, publish_schema, algo_name, algo_hash)
