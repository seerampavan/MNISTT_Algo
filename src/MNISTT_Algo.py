from Algorithmia import ADK
import torch
from torchvision import transforms
import json
from PIL import Image

# This example utilize a trained model to predict the class of an image
# Using the Fashion MNIST dataset

# The model itself is stored in an Algorithmia Data Collection; that is defined in the model_manifest.json file
def load(state):
    # We load the model in this way to ensure that the model is loaded only once
    l = state.get_model('fashion_mnist')
    state['model'] = torch.jit.load(l)
    with open(state.get_model('labels_map')) as f:
        state['labels'] = json.load(f)

    return state

# As we're using the ADK system, state is a dictionary that contains the model and the labels

def apply(input, state):
    local_img = state.client.file(input).getFile(as_path=True)

    transform = transforms.Compose([transforms.Resize(28),
                                    transforms.ToTensor()])
    img = Image.open(local_img).convert('L')
    tensor = transform(img)
    output = state['model'].forward(tensor)
    _, predicted = torch.max(output.data, 1)
    prediction = state['labels'][int(predicted.item())]
    return prediction


algorithm = ADK(apply, load)
algorithm.init("data://zeryx/mnist_examples/mnist_4.png")
