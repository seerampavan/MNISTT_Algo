import requests
import time


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Simple " + self.token
        return r


def get_build_id(api_key, api_address, algo_name, hash, marker=None):
    if marker:
        url = "{}/v1/algorithms/{}/builds?limit={}&marker={}".format(api_address, algo_name, 10, marker)
    else:
        url = "{}/v1/algorithms/{}/builds?limit={}".format(api_address, algo_name, 10)
    result = get_api_request(url, api_key, algo_name)
    builds = result['results']
    for build in builds:
        if hash in build['commit_sha']:
            build_id = build['build_id']
            return build_id
    marker = result['marker']
    return get_build_id(api_key, api_address, algo_name, hash, marker)


def wait_for_result(api_key, api_address, algo_name, build_id):
    waiting = True
    url = "{}/v1/algorithms/{}/builds/{}".format(api_address, algo_name, build_id)
    url_logs = "{}/v1/algorithms/{}/builds/{}/logs".format(api_address, algo_name, build_id)
    print(url)
    while waiting:
        result = get_api_request(url, api_key, algo_name)
        if result['status'] == 'in-progress':
            pass
        elif result['status'] == 'succeeded':
            waiting = False
        else:
            log_data = get_api_request(url_logs, api_key, algo_name)
            raise Exception("build failure:\n{}".format(log_data['logs']))
        time.sleep(1)


def get_api_request(url, api_key, algo_name):
    response = requests.get(auth=BearerAuth(api_key), url=url)
    if response.status_code == 200:
        return response.json()
    elif response.status_code == 404:
        raise Exception("check 'algo_name' {}, 404 not found".format(algo_name))
    elif response.status_code == 401:
        raise Exception("check 'mgmt_api_key' {}, 401 not authorized".format(api_key))
    else:
        raise Exception("request failed with status: {}".format(response.status_code))


def build_wait(mgmt_key, api_address, algo_name, git_hash):
    print("--- Finding build in progress ---")
    build_id = get_build_id(mgmt_key, api_address, algo_name, git_hash)
    print("--- Build ID found, waiting for result ---")
    time.sleep(5)
    wait_for_result(mgmt_key, api_address, algo_name, build_id)
    print("--- Build successful ---")


def get_build(mgmt_key, api_address, algo_name):
    url = "{}/v1/algorithms/{}/builds?limit={}".format(api_address, algo_name, 1)
    result = get_api_request(url, mgmt_key, algo_name)
    build = result['results'][0]
    return build['commit_sha']
